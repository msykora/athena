#
# Packages to build as part of AnalysisBase:
#
+ AsgExternal/Asg_Test
+ AtlasTest/TestTools
+ Calorimeter/CaloGeoHelpers
+ Control/AthContainersInterfaces
+ Control/AthContainers
+ Control/AthLinksSA
+ Control/AthToolSupport/.*
+ Control/CxxUtils
+ Control/xAODRootAccess.*
+ DetectorDescription/GeoPrimitives
+ DetectorDescription/IRegionSelector
+ DetectorDescription/RoiDescriptor
+ Event/EventPrimitives
+ Event/FourMomUtils
- Event/xAOD/.*AthenaPool
- Event/xAOD/.*Cnv
+ Event/xAOD/.*
+ Generators/TruthUtils
+ MuonSpectrometer/MuonIdHelpers
+ Tools/PathResolver
+ Trigger/TrigConfiguration/TrigConfBase
+ Trigger/TrigConfiguration/TrigConfL1Data
+ Trigger/TrigConfiguration/TrigConfHLTData
+ Trigger/TrigEvent/TrigNavStructure
- .*
